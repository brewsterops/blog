---
title: "Running Inlets Pro as a Service"
date: 2020-05-21
description: "Evolution of Inlets Pro usage... and making things just work."
tags: [Inlets-pro, Inlets, Homelab]
---
[Inlets Pro](https://github.com/inlets/inlets-pro) is awesome - I wrote [in depth](../inlets-pro-homelab/) about how I'm using it in my day to day life. That was all well and good, but then I [built a new computer](../i-built-a-computer/) and started rebooting _a whole bunch_. It suddenly became a PITA to constantly go search for my inlets connection command to bring things back up. I needed a quick and easy way to persist my configuration(s) and have the tunnel come up without me having to do anything. 
![Automate All The Things](/inlets-service/automate-all-the-things.jpg)

## Systemd to the rescue
Queue reason #1,000 why just running Linux is awesome. I'm sure you can do a similar thing in Mac or Windows, but it is undoubtedly harder to do.

The only thing you really need to do in order to make this _really_ painless is write a systemd unit file. There's [plenty of great tutorials](https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files) out there for this sort of thing, so I'll spare you an in depth explainer. I'm sure others can, do, and will explain the intricacies of systemd far better than I can, but here's what works for Inlets Pro:

_**Caveat: This assumes you are on a distro that uses systemd. The following is tested on Ubuntu 20.04. YMMV**_
```
[Unit]
Description=Inlets Connection
After=sshd.service

[Service]
EnvironmentFile=-/etc/default/inlets-pro
ExecStart=/usr/local/bin/inlets-pro client --connect "wss://${IP}:8123/connect" --token "$TOKEN" --license "$LICENSE" --tcp-ports $TCP_PORTS
ExecReload=/bin/kill -CONT $MAINPID
KillMode=process
Restart=on-failure
RestartSec=10
RestartPreventExitStatus=255
Type=simple
RuntimeDirectory=inlets
RuntimeDirectoryMode=0755

[Install]
WantedBy=multi-user.target
Alias=inlets.service
```
There are a couple of key items I had to get set right before it all worked:
* `After`: - I know we need _at least_ sshd, since this is what I'm using the tunnel for. If you are using other services, set this accordingly.
* `ExecReload`: - must use the `-CONT` signal or it will just die.
* `RestartSec`: - I set it to `10`. If something isn't running yet, probably because I screwed up what is in `After`, inlets will re-try way too fast and die. Giving it some time will (probably) allow whatever you need to come up first to come up.

You also need to create the `EnvironmentFile` at `/etc/default/inlets-pro` (or your filepath of choice). Here's an example:
```
IP="39.217.39.42"
TOKEN=yourtoken
LICENSE=yourinletslicensestring
TCP_PORTS=2222
```
That's pretty much all there is to it! Just drop the systemd unit file in `/etc/systemd/system/` and name it something like `inlets.service` and you are off to the races. 

Checking the status is easy:
```
matt@beaverdam:~/dev/blog$ service inlets status
● inlets.service - Inlets Connection
     Loaded: loaded (/etc/systemd/system/inlets.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2020-05-20 22:30:51 MDT; 26min ago
    Process: 184445 ExecReload=/bin/kill -CONT $MAINPID (code=exited, status=0/SUCCESS)
   Main PID: 184398 (inlets-pro)
      Tasks: 14 (limit: 154448)
     Memory: 7.7M
     CGroup: /system.slice/inlets.service
             └─184398 /usr/local/bin/inlets-pro client --connect wss://39.217.39.42:8123/connect --token myawesometoken

May 20 22:30:51 beaverdam inlets-pro[184398]: 2020/05/20 22:30:51 Welcome to inlets-pro!
May 20 22:30:51 beaverdam inlets-pro[184398]: 2020/05/20 22:30:51 Starting client - version 0.6.0
May 20 22:30:51 beaverdam inlets-pro[184398]: 2020/05/20 22:30:51 Licensed to: Matt Brewster <brewsterops@gmail.com>, expires: 332 day(s)
May 20 22:30:51 beaverdam inlets-pro[184398]: 2020/05/20 22:30:51 TCP Ports: [2222]
May 20 22:30:51 beaverdam inlets-pro[184398]: inlets-pro client. Copyright Alex Ellis, OpenFaaS Ltd 2020
May 20 22:30:51 beaverdam inlets-pro[184398]: time="2020-05-20T22:30:51-06:00" level=info msg="Connecting to proxy" url="wss://39.217.39.42:8123/connect"
```

## RTFM
Alex pointed out to me that you can actually generate the service file with the inlets-pro binary. 
![Tweet](/inlets-service/inlets-service-twitter.png)

To do so, just run the following:
```
matt@beaverdam:~/dev$ inlets-pro client --license="yourlicensekey" --generate systemd
2020/05/21 13:02:52 Welcome to inlets-pro!
2020/05/21 13:02:52 Starting client - version 0.6.0
[Unit]
Description=inlets-pro Client Tunnel Service
After=network.target

[Service]
Type=simple
Restart=always
RestartSec=5
StartLimitInterval=0
ExecStart=/usr/local/bin/inlets-pro client --connect "ws://localhost:8123/connect" --auto-tls --tcp-ports="80,443" --license=yourlicensekey --token=""

[Install]
WantedBy=multi-user.target
```

You will of course still need to set your token, the appropriate IP, and the tcp ports you want to use. Alternatively, you can use the approach with an `EnvironmentFile` as I mentioned above. The rest of the steps still apply.

You can now reboot at will, and your inlets tunnel will come up without you having to do anything. Nada, zero, zilch. Automation bliss.


