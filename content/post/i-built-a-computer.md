---
title: "I Built a Computer! - Part 1"
date: 2020-05-19
description: "It's been a longggg time (maybe high school?) since I've touched actual hardware. It was time for that to change."
tags: [Ryzen9, Homelab]
---
I've been playing around with some tools for running many [k3s](https://k3s.io/) edge "clusters" at scale for a side project I'm poking around with and started looking in to what it would cost to spin up a bunch of instances in the cloud to test things out. Long story short, you can pretty quickly just pay for a new box. Me being me, I convinced myself that I suddenly *needed* to build a HEDT (nerd speak for high end desktop). Plus, I occasionally edit some photos and like to think that I have time to play games (haha - with a one year old?!). **What more justification did I need? None.**

## Memory Lane
Truth be told, I haven't had a desktop since I set off for college back in 2003. Somewhere along the lines I lost the interest/desire to mess around with hardware, and a laptop was good enough. I used to _really enjoy_ pouring through hardware specs and reading up on the latest and greatest, so this project was somewhat of a walk down memory lane. I missed it.

## "Requirements"
Ok, so I had a few basic goals for my build, namely:
1. It must be relatively quiet
2. It must run lots of things in parallel
3. It must cost less than $2,000 (not interested in a divorce)

Off I went, scouring the interwebs and spending _way too much_ time figuring out what would give me those marginal gains I obviously couldn't live without. Mostly because it's how the 16 year old me did it (woah that was before Amazon existed), I obviously went to Newegg and started putting together the list.

**_Many hours later, this is what I ended up with_**

![Build Parts](/i-built-a-computer/computer_build_parts.jpg)

## Bill of Materials
* CPU: [Ryzen 9 3900x](https://www.newegg.com/amd-ryzen-9-3900x/p/N82E16819113103?Item=N82E16819113103)
* RAM: [Corsair Vengeance LPX 64GB 3600 DDR4](https://www.newegg.com/corsair-64gb-288-pin-ddr4-sdram/p/N82E16820236592?Item=N82E16820236592)
* Storage: [1GB Intel NVME SSD](https://www.newegg.com/intel-660p-series-1tb/p/N82E16820167462?Item=N82E16820167462)
* Video Card: [Radeon 5600XT](https://www.newegg.com/sapphire-radeon-rx-5600-100419p6gl/p/N82E16814202364?Item=N82E16814202364)
* Mother Board: [ASRock x570 Taichi](https://www.newegg.com/asrock-x570-taichi/p/N82E16813157883?Item=N82E16813157883)
* Case: [Fractal Meshify C](https://www.newegg.com/black-fractal-design-meshify-c-atx-mid-tower/p/N82E16811352085?Item=N82E16811352085)
* PSU: [EVGA 650 G5](https://www.newegg.com/evga-supernova-650-g5-220-g5-0650-x1-650w/p/N82E16817438163?Item=N82E16817438163)
* Cooler: [Bequiet! Dark Rock Pro 4](https://www.newegg.com/be-quiet-dark-rock-pro-4-bk022/p/13C-001F-00027?Item=9SIA68V6YA3005)
--------------------
**Total Price:** $1,898.01

## Justifications
* The Ryzen 9 3900x is basically impossible to beat on a performance/$ basis. It has _12 cores/24 threads_. Go look up what a 24vCPU instance costs you to run on the cloud. 
* I wanted enough RAM to run _many_ VM's. Kubernetes is memory hungry. Especially if you are running lots of clusters. I knew I wanted 64GB, and wasn't interested in flashy RGB effects, so the Corsair fit the bill.**
* Storage-wise, I already had a fancy Samsung NVMe stick that I planned on harvesting from my NUC that the OS was going to be installed on, so I went with the Intel NVME SSD for its price point. I'd mostly be using it for photo/video storage.**
* For a video card, I knew I wanted the _ability_ to play some games (occasionally I'll get lost in Forza Horizon or Descenders), but couldn't really justify a high end card for how much it was going to get used. (See 2k budget and lack of desire for divorce). The Radeon 5600xt fit the bill nicely at the $300 mark.
* All of this stuff has to get bolted to something. The ASRock Taichi offered 3 NVMe slots, Wifi AX (I don't have a wired connection in my office... grr...) and reviews seemed good, so went with it. I don't care much about overclocking (and value stability), so wasn't chasing after those features.
* I wanted an understated appearance that maximized cooling. The Fractal Meshify looks nice without standing out too much. It's also not _huge_. It sits quietly under my desk without attracting too much attention.
* After reading a bunch of reviews, it was apparent that the additional complexity (and expense) of water cooling was completely unnecessary. I could have used the bundled cooler with the CPU, but wanted it to be quiet. What better brand than _Be quiet!_ to fit the bill. This $95 splurge was well worth it. I also went with a PSU that had an eco mode and provided plenty of power for my parts list.

![Build In Progress](/i-built-a-computer/build_in_progress.jpg)

** _For those eagle eyed viewers, you probably realized that there are 2 packages of RAM and 2 SSD's. They accidentally sent me doubles of those items, and I just couldn't bring myself to send them back. Who *doesn't* want 128GB of RAM and 2TB of storage._ `¯\_(ツ)_/¯`

## Initial Thoughts
So far, I've been super happy with what I ended up with. This system is _screaming_ fast, and probably (definitely?) overkill. It's still early days, and I'm still getting things set up just the way I want. Stay tuned for Part 2 on OS, tooling, configuration, etc. Happy building!