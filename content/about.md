---
title: About me
subtitle: Musings of Brewsterops
date: 2020-04-17T02:01:58+05:30
comments: false
---

My name is Matt Brewster. I've been wandering about the dev and devops world for over a decade now, and decided I should start sharing useful things I figure out. Some interesting tid-bits about me:

- I work primarly remotely from the mountains of Idaho. Check out [McCall](https://en.wikipedia.org/wiki/McCall,_Idaho). The lifestyle isn't for everyone, but it is for me!
- I like tech, but I also like getting outside (see above).
- Our family grew by one in 2019, and life changed forever!

Welcome to my Blog. I hope you find something useful.

### my history

I started my career out working for Boeing **way** back in 2007 after attending [Washington State University](https://www.wsu.edu) (Go Cougs)! My time there was awesome, and I truly think I got a first-rate practical education in the [MIS](https://business.wsu.edu/departments/mise/) program. Most of my time in the industry has been spent figuring out how to bring new tech and tools to industries that are reluctant to change their ways. Since around 2009, I've worked in the consulting space. I've been both an IC and lead, and enjoy different parts of both roles. My next adventure will probably include a trip back to the product space.
